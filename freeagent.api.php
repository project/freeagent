<?php

/**
 * @file
 * Callbacks and hooks related to FreeAgent.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * React to the refresh of data from FreeAgent.
 */
function hook_freeagent_refresh_data() {
  // React to the latest FreeAgent data refresh.
}

/**
 * Do additional processing on the new or modified users imported.
 *
 * @param $users
 *   An array of all the latest FreeAgent users imported.
 */
function hook_freeagent_fetch_users(array $users) {
}

/**
 * Do additional processing on the new or modified contacts imported.
 *
 * @param $contacts
 *   An array of all the latest FreeAgent contacts imported.
 */
function hook_freeagent_fetch_contacts(array $contacts) {
}

/**
 * Do additional processing on the new or modified projects imported.
 *
 * @param $projects
 *   An array of all the latest FreeAgent projects imported.
 */
function hook_freeagent_fetch_projects(array $projects) {
}

/**
 * Do additional processing on the new or modified tasks imported.
 *
 * @param $tasks
 *   An array of all the latest FreeAgent tasks imported.
 */
function hook_freeagent_fetch_tasks(array $tasks) {
}

/**
 * Do additional processing on the new or modified timeslips imported.
 *
 * @param $timeslips
 *   An array of all the latest FreeAgent timeslips imported.
 */
function hook_freeagent_fetch_timeslips(array $timeslips) {
}

/**
 * Do additional processing on the new or modified invoices imported.
 *
 * @param $invoices
 *   An array of all the latest FreeAgent invoices imported.
 */
function hook_freeagent_fetch_invoices(array $invoices) {
}

/**
 * Do additional processing on the new or modified categories imported.
 *
 * @param $categories
 *   An array of all the latest FreeAgent categories imported.
 */
function hook_freeagent_fetch_categories(array $categories) {
}

/**
 * Do additional processing on the new or modified expenses imported.
 *
 * @param $expenses
 *   An array of all the latest FreeAgent expenses imported.
 */
function hook_freeagent_fetch_expenses(array $expenses) {
}

/**
 * Do additional processing on the new or modified bills imported.
 *
 * @param $bills
 *   An array of all the latest FreeAgent bills imported.
 */
function hook_freeagent_fetch_bills(array $bills) {
}

/**
 * @} End of "addtogroup hooks".
 */

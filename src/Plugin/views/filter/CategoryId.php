<?php

namespace Drupal\freeagent\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Provides filter by FreeAgent category id.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("freeagent_category_id")
 */
class CategoryId extends InOperator {

  use FilterTrait;

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (!isset($this->valueOptions)) {
      $this->valueTitle = t('Categories');
      $categories = $this->freeAgent->getCategoriesInfo();
      $options = [];
      foreach ($categories as $id => $info) {
        $options[$id] = $info->description;
      }
      natcasesort($options);
      $this->valueOptions = $options;
    }
  }

}

<?php

namespace Drupal\freeagent\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Provides filter by FreeAgent contact id.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("freeagent_contact_id")
 */
class ContactId extends InOperator {

  use FilterTrait;

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (!isset($this->valueOptions)) {
      $this->valueTitle = t('Contacts');
      $contacts = $this->freeAgent->getContactsInfo();
      $options = [];
      foreach ($contacts as $id => $info) {
        $options[$id] = $info->contact_name;
      }
      natcasesort($options);
      $this->valueOptions = $options;
    }
  }

}

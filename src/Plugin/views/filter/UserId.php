<?php

namespace Drupal\freeagent\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Provides filter by FreeAgent user id.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("freeagent_user_id")
 */
class UserId extends InOperator {

  use FilterTrait;

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (!isset($this->valueOptions)) {
      $this->valueTitle = t('Users');
      $users = $this->freeAgent->getUsersInfo();
      $options = [];
      foreach ($users as $id => $info) {
        $options[$id] = $info->full_name;
      }
      natcasesort($options);
      $this->valueOptions = $options;
    }
  }

}

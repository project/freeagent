<?php

namespace Drupal\freeagent\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Provides filter by FreeAgent task id.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("freeagent_task_id")
 */
class TaskId extends InOperator {

  use FilterTrait;

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (!isset($this->valueOptions)) {
      $this->valueTitle = t('Tasks');
      $tasks = $this->freeAgent->getTasksInfo();
      $options = [];
      foreach ($tasks as $id => $info) {
        $options[$id] = $info->contact_name . ': ' . $info->project_name . ': ' . $info->name;
      }
      natcasesort($options);
      $this->valueOptions = $options;
    }
  }

}

<?php

namespace Drupal\freeagent\Plugin\views\filter;

use Drupal\freeagent\Services\FreeAgent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides common methods and services for filter handlers.
 */
trait FilterTrait {

  /**
   * The FreeAgent service.
   *
   * @var \Drupal\freeagent\Services\FreeAgent
   */
  protected $freeAgent;

  /**
   * Constructs a new FilterTrait instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\freeagent\Services\FreeAgent $freeagent
   *   The FreeAgent service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FreeAgent $freeagent) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->freeAgent = $freeagent;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('freeagent')
    );
  }

}

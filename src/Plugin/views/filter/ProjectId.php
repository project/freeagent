<?php

namespace Drupal\freeagent\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Provides filter by FreeAgent project id.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("freeagent_project_id")
 */
class ProjectId extends InOperator {

  use FilterTrait;

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (!isset($this->valueOptions)) {
      $this->valueTitle = t('Projects');
      $projects = $this->freeAgent->getProjectsInfo();
      $options = [];
      foreach ($projects as $id => $info) {
        $options[$id] = $info->contact_name . ': ' . $info->name;
      }
      natcasesort($options);
      $this->valueOptions = $options;
    }
  }

}

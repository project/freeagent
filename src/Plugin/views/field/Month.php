<?php

namespace Drupal\freeagent\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\EntityField;
use Drupal\views\ResultRow;

/**
 * Provides field to render month label.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("freeagent_month")
 */
class Month extends EntityField {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['use_month_label'] = ['default' => FALSE, 'bool' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['use_month_label'] = [
      '#type' => 'checkbox',
      '#title' => t('Display month label'),
      '#description' => t('If checked, the month name will be displayed instead of the number.'),
      '#default_value' => $this->options['use_month_label'],
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);

    if (!empty($this->options['use_month_label'])) {
      $months = [
        1 => t('Jan'),
        2 => t('Feb'),
        3 => t('Mar'),
        4 => t('Apr'),
        5 => t('May'),
        6 => t('Jun'),
        7 => t('Jul'),
        8 => t('Aug'),
        9 => t('Sep'),
        10 => t('Oct'),
        11 => t('Nov'),
        12 => t('Dec'),
      ];
      if (!empty($months[$value])) {
        $value = $months[$value];
      }
    }

    return $this->sanitizeValue($value);
  }

}

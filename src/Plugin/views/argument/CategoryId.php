<?php

namespace Drupal\freeagent\Plugin\views\argument;

use Drupal\Component\Utility\Html;
use Drupal\views\Plugin\views\argument\NumericArgument;

/**
 * Provides argument to accept FreeAgent category id.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("freeagent_category_id")
 */
class CategoryId extends NumericArgument {

  use ArgumentTrait;

  /**
   * {@inheritdoc}
   */
  public function titleQuery() {
    $names = [];
    $query = $this->connection->select('freeagent_categories', 'c');
    $query->addField('c', 'description');
    $query->condition('c.id', $this->value, 'IN');
    foreach ($query->execute() as $term) {
      $names[] = Html::escape($term->description);
    }
    return $names;
  }

}

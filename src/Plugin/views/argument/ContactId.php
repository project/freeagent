<?php

namespace Drupal\freeagent\Plugin\views\argument;

use Drupal\Component\Utility\Html;
use Drupal\views\Plugin\views\argument\NumericArgument;

/**
 * Provides argument to accept FreeAgent contact id.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("freeagent_contact_id")
 */
class ContactId extends NumericArgument {

  use ArgumentTrait;

  /**
   * {@inheritdoc}
   */
  public function titleQuery() {
    $names = [];
    $query = $this->connection->select('freeagent_contacts', 'c');
    $query->addField('c', 'organisation_name');
    $query->condition('c.id', $this->value, 'IN');
    foreach ($query->execute() as $term) {
      $names[] = Html::escape($term->organisation_name);
    }
    return $names;
  }

}

<?php

namespace Drupal\freeagent\Plugin\views\argument;

use Drupal\Component\Utility\Html;
use Drupal\views\Plugin\views\argument\NumericArgument;

/**
 * Provides argument to accept FreeAgent task id.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("freeagent_task_id")
 */
class TaskId extends NumericArgument {

  use ArgumentTrait;

  /**
   * {@inheritdoc}
   */
  public function titleQuery() {
    $names = [];
    $query = $this->connection->select('freeagent_tasks', 't');
    $query->addField('t', 'name');
    $query->condition('t.id', $this->value, 'IN');
    foreach ($query->execute() as $term) {
      $names[] = Html::escape($term->name);
    }
    return $names;
  }

}

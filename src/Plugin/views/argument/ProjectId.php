<?php

namespace Drupal\freeagent\Plugin\views\argument;

use Drupal\Component\Utility\Html;
use Drupal\views\Plugin\views\argument\NumericArgument;

/**
 * Provides argument to accept FreeAgent project id.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("freeagent_project_id")
 */
class ProjectId extends NumericArgument {

  use ArgumentTrait;

  /**
   * {@inheritdoc}
   */
  public function titleQuery() {
    $names = [];
    $query = $this->connection->select('freeagent_projects', 'p');
    $query->addField('p', 'name');
    $query->condition('p.id', $this->value, 'IN');
    foreach ($query->execute() as $term) {
      $names[] = Html::escape($term->name);
    }
    return $names;
  }

}

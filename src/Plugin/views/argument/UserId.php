<?php

namespace Drupal\freeagent\Plugin\views\argument;

use Drupal\Component\Utility\Html;
use Drupal\views\Plugin\views\argument\NumericArgument;

/**
 * Provides argument to accept FreeAgent user id.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("freeagent_user_id")
 */
class UserId extends NumericArgument {

  use ArgumentTrait;

  /**
   * {@inheritdoc}
   */
  public function titleQuery() {
    $names = [];
    $query = $this->connection->select('freeagent_users', 'u');
    $query->addField('u', 'full_name');
    $query->condition('u.id', $this->value, 'IN');
    foreach ($query->execute() as $term) {
      $names[] = Html::escape($term->full_name);
    }
    return $names;
  }

}

<?php

namespace Drupal\freeagent\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\freeagent\Services\FreeAgent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides controllers for FreeAgent API module.
 */
class FreeAgentController extends ControllerBase {

  /**
   * The FreeAgent service.
   *
   * @var \Drupal\freeagent\Services\FreeAgent
   */
  protected $freeAgent;

  /**
   * Constructs a new FreeAgentController instance.
   *
   * @param \Drupal\freeagent\Services\FreeAgent $freeagent
   *   The FreeAgent service.
   */
  public function __construct(FreeAgent $freeagent) {
    $this->freeAgent = $freeagent;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('freeagent')
    );
  }

  /**
   * Authorise the site against FreeAgent API.
   *
   * @param string $request_type
   *   (optional) The request type.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The controller response.
   */
  public function authorise($request_type = NULL) {
    return $this->freeAgent->authorise($request_type);
  }

}

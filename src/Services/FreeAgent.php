<?php

namespace Drupal\freeagent\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use OAuth2\Client;
use Psr\Log\LoggerInterface;

/**
 * Provides utility functions for FreeAgent API module.
 */
class FreeAgent {

  /**
   * The url endpoint to use.
   */
  private $endpoint;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The freeagent.settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new FreeAgent instance.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $config_factory, StateInterface $state, CacheBackendInterface $cache, MessengerInterface $messenger, LoggerInterface $logger, ModuleHandlerInterface $module_handler) {
    $this->connection = $connection;
    $this->config = $config_factory->get('freeagent.settings');
    $this->state = $state;
    $this->cache = $cache;
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;

    $use_sandbox = $this->config->get('use_sandbox');
    $this->endpoint = $use_sandbox ? 'https://api.sandbox.freeagent.com' : 'https://api.freeagent.com';
  }

  /**
   * Authorise the site against FreeAgent API.
   *
   * @param string $request_type
   *   (optional) The request type.
   *
   * @return \Symfony\Component\HttpFoundation\Response|null
   *   The authorisation response, if available.
   */
  public function authorise($request_type = NULL) {
    $client_id = $this->config->get('client_id');
    $client_secret = $this->config->get('client_secret');
    if (empty($client_id) || empty($client_secret)) {
      $this->messenger->addError(t('Please configure your app client id and secret.'));
      $url = Url::fromRoute('freeagent.settings')->setAbsolute()->toString(TRUE)->getGeneratedUrl();
      return new TrustedRedirectResponse($url);
    }

    $redirect_url = Url::fromRoute('freeagent.settings')->setAbsolute()->toString(TRUE)->getGeneratedUrl();
    $authorization_endpoint = $this->endpoint . '/v2/approve_app';
    $token_endpoint = $this->endpoint . '/v2/token_endpoint';

    $client = new Client($client_id, $client_secret);

    // Perform a refresh token request.
    if ($request_type == 'refresh_token') {
      $refresh_token = $this->state->get('freeagent.refresh_token', '');
      $params = [
        'refresh_token' => $refresh_token,
      ];
      $response = $client->getAccessToken($token_endpoint, 'refresh_token', $params);
      if (!empty($response['result']['error'])) {
        $this->messenger->addError(t('Failed to retrieve access token, error: @code: @error', [
          '@code' => $response['code'],
          '@error' => $response['result']['error'],
        ]));
      }
      else {
        $time = time();
        $this->state->setMultiple([
          'freeagent.access_token' => $response['result']['access_token'],
          'freeagent.token_type' => $response['result']['token_type'],
          'freeagent.token_expiry' => $time + $response['result']['expires_in'],
        ]);
        return NULL;
      }
    }

    // Perform an authorization request.
    elseif (!isset($_GET['code'])) {
      $url = $client->getAuthenticationUrl($authorization_endpoint, $redirect_url);
      return new TrustedRedirectResponse($url);
    }

    // Get an access token.
    else {
      $params = [
        'code' => $_GET['code'],
        'redirect_uri' => $redirect_url,
      ];
      $response = $client->getAccessToken($token_endpoint, 'authorization_code', $params);
      if (!empty($response['result']['error'])) {
        $this->messenger->addError(t('Failed to retrieve access token, error: @code: @error', [
          '@code' => $response['code'],
          '@error' => $response['result']['error'],
        ]));
      }
      else {
        $time = time();
        $this->state->setMultiple([
          'freeagent.access_token' => $response['result']['access_token'],
          'freeagent.token_type' => $response['result']['token_type'],
          'freeagent.refresh_token' => $response['result']['refresh_token'],
          'freeagent.token_expiry' => $time + $response['result']['expires_in'],
        ]);
        $this->messenger->addStatus(t('App successfully authorised.'));
        $url = Url::fromRoute('freeagent.settings')->setAbsolute()->toString(TRUE)->getGeneratedUrl();
        return new TrustedRedirectResponse($url);
      }
    }
    return NULL;
  }

  /**
   * Send a request to FreeAgent.
   */
  public function request($url, $action, &$num_requests, $params = []) {
    // Rate limiting in place, so delay the next request for 60 seconds.
    // Max 120 requests in 1 minute.
    // As this resets at the top of each minute, try delaying for 30 seconds.
    if ($num_requests != 0 && $num_requests % 100 == 0) {
      $this->messenger->addStatus("$num_requests: sleeping for 60");
      $this->logger->notice("$num_requests: sleeping for 60");
      sleep(60);
    }
    $debug = $this->state->get('freeagent.debug', FALSE);
    if ($debug) {
      $this->logger->debug('url: @url', ['@url' => $url]);
    }

    $time = time();
    $client_id = $this->config->get('client_id');
    $client_secret = $this->config->get('client_secret');
    $access_token = $this->state->get('freeagent.access_token', '');
    $token_expiry = $this->state->get('freeagent.token_expiry', $time);

    // Ensure we have the necessary info.
    if (empty($client_id) || empty($client_secret) || empty($access_token)) {
      $this->messenger->addError(t('Please authorise your app client id and secret to get an access token.'));
      $url = Url::fromRoute('freeagent.settings')->setAbsolute()->toString(TRUE)->getGeneratedUrl();
      return new TrustedRedirectResponse($url);
    }

    // Check if we need to refresh our access token.
    if ($token_expiry <= $time) {
      $this->authorise('refresh_token');
      $access_token = $this->state->get('freeagent.access_token', '');
    }
    $token_type = $this->state->get('freeagent.token_type', 'bearer');
    switch ($token_type) {
      case 'mac':
        $access_token_type = 3;
        break;
      case 'uri':
        $access_token_type = 0;
        break;
      case 'oauth':
        $access_token_type = 2;
        break;
      case 'bearer':
      default:
        $access_token_type = 1;
        break;
    }

    $client = new Client($client_id, $client_secret);
    $client->setAccessToken($access_token);
    $client->setAccessTokenType($access_token_type);

    $http_headers = [
      'User-Agent' => 'Drupal (+http://drupal.org/)',
    ];

    // For GET, $params will be empty by default so we can override it.
    $base_url = $url;
    if ($action == 'GET') {
      $url_parts = explode('?', $url);
      $base_url = $url_parts[0];
      if (!empty($url_parts[1])) {
        $params = $url_parts[1];
      }
    }
    else {
      $params = json_encode($params);
      $http_headers['Content-Type'] = 'application/json';
    }
    $response = $client->fetch($base_url, $params, $action, $http_headers);

    if ($debug) {
      $this->logger->debug('response: @response', ['@response' => print_r($response, TRUE)]);
    }

    return $response;
  }

  /**
   * Parse a date and reformat it as a unix timestamp.
   *
   * @param $date
   *   A date in the format 2011-08-16T13:32:00Z
   */
  private function parseDate($date) {
    if (empty($date)) {
      return 0;
    }
    if (strpos($date, 'T') !== FALSE) {
      $parts = explode('T', trim($date, 'Z')); // 2011-08-16T13:32:00Z
      $date_parts = explode('-', $parts[0]);
      $time_parts = explode(':', $parts[1]);
      $timestamp = mktime($time_parts[0], $time_parts[1], $time_parts[2], $date_parts[1], $date_parts[2], $date_parts[0]);
    }
    else {
      $date_parts = explode('-', $date);
      $timestamp = mktime(0, 0, 0, $date_parts[1], $date_parts[2], $date_parts[0]);
    }
    return $timestamp;
  }

  /**
   * Parse the HTTP headers and return the "Next" page link if available.
   *
   * @param $headers
   *   An array of HTTP headers.
   *
   * @return
   *   The url of the "next" page if present, NULL otherwise.
   */
  private function getNextPageLink($headers) {
    // Parse the headers looking for the "Link" header.
    foreach ($headers as $header) {
      if (stripos($header, 'Link:') !== FALSE) {
        $links = explode(',', $header);
        if (!empty($links)) {
          foreach ($links as $link) {
            // If we have a "next" link, fetch its data.
            if (stripos($link, "rel='next'") !== FALSE) {
              $start = strpos($link, '<');
              $end = strpos($link, '>');
              $length = $end - $start - 1;
              return substr($link, $start + 1, $length);
            }
          }
        }
      }
    }
    return NULL;
  }

  /**
   * Purge FreeAgent data from the database.
   *
   * @param $data_types
   *   Array of data types to purge.
   */
  public function purgeData($data_types = []) {
    $purge_all = empty($data_types);

    if ($purge_all || in_array('contacts', $data_types)) {
      $contacts_count = $this->connection->delete('freeagent_contacts')->execute();
      if (!empty($contacts_count)) {
        $this->messenger->addStatus(t('Deleted @count contacts.', ['@count' => $contacts_count]));
      }
    }
    if ($purge_all || in_array('projects', $data_types)) {
      $projects_count = $this->connection->delete('freeagent_projects')->execute();
      if (!empty($projects_count)) {
        $this->messenger->addStatus(t('Deleted @count projects.', ['@count' => $projects_count]));
      }
    }
    if ($purge_all || in_array('tasks', $data_types)) {
      $tasks_count = $this->connection->delete('freeagent_tasks')->execute();
      if (!empty($tasks_count)) {
        $this->messenger->addStatus(t('Deleted @count tasks.', ['@count' => $tasks_count]));
      }
    }
    if ($purge_all || in_array('timeslips', $data_types)) {
      $timeslips_count = $this->connection->delete('freeagent_timeslips')->execute();
      if (!empty($timeslips_count)) {
        $this->messenger->addStatus(t('Deleted @count timeslips.', ['@count' => $timeslips_count]));
      }
    }
    if ($purge_all || in_array('invoices', $data_types)) {
      $invoices_count = $this->connection->delete('freeagent_invoices')->execute();
      if (!empty($invoices_count)) {
        $this->messenger->addStatus(t('Deleted @count invoices.', ['@count' => $invoices_count]));
      }
    }
    if ($purge_all || in_array('bills', $data_types)) {
      $bills_count = $this->connection->delete('freeagent_bills')->execute();
      if (!empty($bills_count)) {
        $this->messenger->addStatus(t('Deleted @count bills.', ['@count' => $bills_count]));
      }
    }
    if ($purge_all || in_array('expenses', $data_types)) {
      $expenses_count = $this->connection->delete('freeagent_expenses')->execute();
      if (!empty($expenses_count)) {
        $this->messenger->addStatus(t('Deleted @count expenses.', ['@count' => $expenses_count]));
      }
      $categories_count = $this->connection->delete('freeagent_categories')->execute();
      if (!empty($categories_count)) {
        $this->messenger->addStatus(t('Deleted @count expense categories.', ['@count' => $categories_count]));
      }
    }
  }

  /**
   * Refresh stored FreeAgent data.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   */
  public function refreshData($last_update = 0) {
    if (!$last_update) {
      $last_update = $this->state->get('freeagent.cron_last', 0);
    }
    // Take away 1 day from $last_update to account for timezone issues with the
    // API.
    $last_update -= 86400;

    $success = TRUE;
    $total_requests = $error_count = 0;

    $data_types = $this->config->get('fetch_data_types');

    $this->logger->notice('Fetching users...');
    $num_requests = 0;
    $result = $this->fetchUsers($last_update, $num_requests);
    $total_requests += $num_requests;
    if (!$result) {
      $success = FALSE;
      $error_count++;
    }
    $this->logger->notice('User API, no. calls: ' . $num_requests);

    $this->logger->notice('Fetching contacts...');
    $num_requests = 0;
    $result = $this->fetchContacts($last_update, $num_requests);
    $total_requests += $num_requests;
    if (!$result) {
      $success = FALSE;
      $error_count++;
    }
    $this->logger->notice('Contacts API, no. calls: ' . $num_requests);

    $this->logger->notice('Fetching projects...');
    $num_requests = 0;
    $result = $this->fetchProjects($last_update, $num_requests);
    $total_requests += $num_requests;
    if (!$result) {
      $success = FALSE;
      $error_count++;
    }
    $this->logger->notice('Projects API, no. calls: ' . $num_requests);

    $this->logger->notice('Fetching tasks...');
    $num_requests = 0;
    $result = $this->fetchTasks($last_update, $num_requests);
    $total_requests += $num_requests;
    if (!$result) {
      $success = FALSE;
      $error_count++;
    }
    $this->logger->notice('Tasks API, no. calls: ' . $num_requests);

    if (!empty($data_types['timeslips'])) {
      $this->logger->notice('Fetching timeslips...');
      $num_requests = 0;
      $result = $this->fetchTimesheets($last_update, $num_requests);
      $total_requests += $num_requests;
      if (!$result) {
        $success = FALSE;
        $error_count++;
      }
      $this->logger->notice('Timesheets API, no. calls: ' . $num_requests);
    }

    if (!empty($data_types['invoices'])) {
      $this->logger->notice('Fetching invoices...');
      $num_requests = 0;
      $result = $this->fetchInvoices($last_update, $num_requests);
      $total_requests += $num_requests;
      if (!$result) {
        $success = FALSE;
        $error_count++;
      }
      $this->logger->notice('Invoices API, no. calls: ' . $num_requests);
    }

    if (!empty($data_types['expenses'])) {
      $this->logger->notice('Fetching categories...');
      $num_requests = 0;
      $result = $this->fetchCategories($last_update, $num_requests);
      $total_requests += $num_requests;
      if (!$result) {
        $success = FALSE;
        $error_count++;
      }
      $this->logger->notice('Categories API, no. calls: ' . $num_requests);

      $this->logger->notice('Fetching expenses...');
      $num_requests = 0;
      $result = $this->fetchExpenses($last_update, $num_requests);
      $total_requests += $num_requests;
      if (!$result) {
        $success = FALSE;
        $error_count++;
      }
      $this->logger->notice('Expenses API, no. calls: ' . $num_requests);
    }

    if (!empty($data_types['bills'])) {
      $this->logger->notice('Fetching bills...');
      $num_requests = 0;
      $result = $this->fetchBills($last_update, $num_requests);
      $total_requests += $num_requests;
      if (!$result) {
        $success = FALSE;
        $error_count++;
      }
      $this->logger->notice('Bills API, no. calls: ' . $num_requests);
    }

    $this->logger->notice('Total of %total API calls made.', ['%total' => $total_requests]);

    $this->cache->invalidateMultiple([
      'freeagent_users',
      'freeagent_contacts',
      'freeagent_projects',
      'freeagent_tasks',
      'freeagent_categories',
    ]);

    $this->logger->notice('Invoking refresh data...');
    $this->moduleHandler->invokeAll('freeagent_refresh_data');
    $this->logger->notice('Finished.');

    if ($success) {
      $this->messenger->addStatus(t('Finished syncing data from FreeAgent, @total API calls made', ['@total' => $total_requests]));
    }
    else {
      $this->messenger->addError(t('Finished syncing data from FreeAgent, @total API calls made, @count errors encountered', [
        '@total' => $total_requests,
        '@count' => $error_count,
      ]));
      $this->messenger->addError(t('Check the logs for more details.'));
    }

    return $success;
  }

  /**
   * Grab and update all users.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   * @param $num_requests
   *   Tracks the number of requests performed.
   * @param $url
   *   URL to fetch users from - required for recursive behaviour.
   */
  public function fetchUsers($last_update, &$num_requests = 0, $url = NULL) {
    $success = TRUE;
    $url = empty($url) ? $this->endpoint . "/v2/users?per_page=100" : $url;

    $num_requests++;
    $response = $this->request($url, 'GET', $num_requests);

    // Successful response.
    if (is_array($response) && $response['code'] == 200) {
      $users = [];
      foreach ($response['result']['users'] as $i => $user) {
        $user['id'] = substr(strrchr($user['url'], '/'), 1);
        $user['created'] = $this->parseDate($user['created_at']);
        $user['updated'] = $this->parseDate($user['updated_at']);

        // Write the record.
        $query = $this->connection->merge('freeagent_users');
        $query->key('id', $user['id']);
        $query->fields([
          'first_name' => $user['first_name'],
          'last_name' => $user['last_name'],
          'full_name' => $user['first_name'] . ' ' . $user['last_name'],
          'email' => isset($user['email']) ? $user['email'] : '',
          'role' => isset($user['role']) ? $user['role'] : '',
          'permission_level' => !empty($user['permission_level']) ? $user['permission_level'] : 0,
          'opening_mileage' => !empty($user['opening_mileage']) ? $user['opening_mileage'] : 0,
          'created' => $user['created'],
          'updated' => $user['updated'],
        ]);
        $query->execute();
        $users[] = $user;
      }

      // Invoke freeagent_fetch_users hook.
      $this->moduleHandler->invokeAll('freeagent_fetch_users', [$users]);

      // For paginated responses, grab the next page of results.
      $next_url = $this->getNextPageLink($response['headers']);
      if ($next_url) {
        $success = $this->fetchUsers($last_update, $num_requests, $next_url);
      }
    }
    else {
      $success = FALSE;
      $code = empty($response['code']) ? 'no-code' : $response['code'];
      $result = empty($response['result']) ? '' : $response['result'];
      $this->logger->error('Error fetching users %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]);
      $this->messenger->addError(t('Error fetching users: %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]));
    }

    return $success;
  }

  /**
   * Grab and update all contacts.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   * @param $num_requests
   *   Tracks the number of requests performed.
   * @param $url
   *   URL to fetch contacts from - required for recursive behaviour.
   */
  public function fetchContacts($last_update, &$num_requests = 0, $url = NULL) {
    $success = TRUE;
    $url = empty($url) ? $this->endpoint . "/v2/contacts?per_page=100&sort=-updated_at" : $url;

    $num_requests++;
    $response = $this->request($url, 'GET', $num_requests);

    // Successful response.
    if (is_array($response) && $response['code'] == 200) {
      $contacts = [];
      foreach ($response['result']['contacts'] as $i => $contact) {
        $contact['id'] = substr(strrchr($contact['url'], '/'), 1);
        $contact['created'] = $this->parseDate($contact['created_at']);
        $contact['updated'] = $this->parseDate($contact['updated_at']);

        // If the updated_at time on the record is before the last data refresh,
        // skip processing of the rest of the records (which are sorted by
        // updated_at descending).
        if ($updated < $last_update) {
          return $num_requests;
        }

        // Write the record.
        $query = $this->connection->merge('freeagent_contacts');
        $query->key('id', $contact['id']);
        $query->fields([
          'first_name' => isset($contact['first_name']) ? $contact['first_name'] : '',
          'last_name' => isset($contact['last_name']) ? $contact['last_name'] : '',
          'organisation_name' => isset($contact['organisation_name']) ? $contact['organisation_name'] : '',
          'email' => isset($contact['email']) ? $contact['email'] : '',
          'billing_email' => isset($contact['billing_email']) ? $contact['billing_email'] : '',
          'phone_number' => isset($contact['phone_number']) ? $contact['phone_number'] : '',
          'mobile' => isset($contact['mobile']) ? $contact['mobile'] : '',
          'address1' => isset($contact['address1']) ? $contact['address1'] : '',
          'address2' => isset($contact['address2']) ? $contact['address2'] : '',
          'address3' => isset($contact['address3']) ? $contact['address3'] : '',
          'town' => isset($contact['town']) ? $contact['town'] : '',
          'region' => isset($contact['region']) ? $contact['region'] : '',
          'postcode' => isset($contact['postcode']) ? $contact['postcode'] : '',
          'country' => isset($contact['country']) ? $contact['country'] : '',
          'locale' => isset($contact['locale']) ? $contact['locale'] : '',
          'status' => isset($contact['status']) ? $contact['status'] : '',
          'charge_sales_tax' => isset($contact['charge_sales_tax']) ? $contact['charge_sales_tax'] : '',
          'contact_name_on_invoices' => !empty($contact['contact_name_on_invoices']) ? $contact['contact_name_on_invoices'] : 0,
          'account_balance' => !empty($contact['account_balance']) ? $contact['account_balance'] : 0,
          'uses_contact_invoice_sequence' => !empty($contact['uses_contact_invoice_sequence']) ? $contact['uses_contact_invoice_sequence'] : 0,
          'vat_no' => isset($contact['sales_tax_registration_number']) ? $contact['sales_tax_registration_number'] : '',
          'created' => $contact['created'],
          'updated' => $contact['updated'],
        ]);
        $query->execute();
        $contacts[] = $contact;
      }

      // Invoke freeagent_fetch_contacts hook.
      $this->moduleHandler->invokeAll('freeagent_fetch_contacts', [$contacts]);

      // For paginated responses, grab the next page of results.
      $next_url = $this->getNextPageLink($response['headers']);
      if ($next_url) {
        $success = $this->fetchContacts($last_update, $num_requests, $next_url);
      }
    }
    else {
      $success = FALSE;
      $code = empty($response['code']) ? 'no-code' : $response['code'];
      $result = empty($response['result']) ? '' : $response['result'];
      $this->logger->error('Error fetching contacts %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]);
      $this->messenger->addError(t('Error fetching contacts: %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]));
    }

    return $success;
  }

  /**
   * Grab and update all projects.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   * @param $num_requests
   *   Tracks the number of requests performed.
   * @param $url
   *   URL to fetch projects from - required for recursive behaviour.
   */
  public function fetchProjects($last_update, &$num_requests = 0, $url = NULL) {
    $success = TRUE;
    $url = empty($url) ? $this->endpoint . "/v2/projects?per_page=100" : $url;

    $num_requests++;
    $response = $this->request($url, 'GET', $num_requests);

    // Successful response.
    if (is_array($response) && $response['code'] == 200) {
      $projects = [];
      foreach ($response['result']['projects'] as $i => $project) {
        $project['id'] = substr(strrchr($project['url'], '/'), 1);
        $project['contact_id'] = substr(strrchr($project['contact'], '/'), 1);
        $project['created'] = $this->parseDate($project['created_at']);
        $project['updated'] = $this->parseDate($project['updated_at']);

        // Write the record.
        $query = $this->connection->merge('freeagent_projects');
        $query->key('id', $project['id']);
        $query->fields([
          'name' => isset($project['name']) ? $project['name'] : '',
          'contact_id' => $project['contact_id'],
          'status' => isset($project['status']) ? $project['status'] : '',
          'budget_units' => isset($project['budget_units']) ? $project['budget_units'] : '',
          'currency' => isset($project['currency']) ? $project['currency'] : '',
          'billing_period' => isset($project['billing_period']) ? $project['billing_period'] : '',
          'budget' => !empty($project['budget']) ? $project['budget'] : 0,
          'normal_billing_rate' => !empty($project['normal_billing_rate']) ? $project['normal_billing_rate'] : 0,
          'hours_per_day' => !empty($project['hours_per_day']) ? $project['hours_per_day'] : 0,
          'uses_project_invoice_sequence' => !empty($project['uses_project_invoice_sequence']) ? $project['uses_project_invoice_sequence'] : 0,
          'starts_on' => !empty($project['starts_on']) ? $this->parseDate($project['starts_on']) : NULL,
          'ends_on' => !empty($project['ends_on']) ? $this->parseDate($project['ends_on']) : NULL,
          'created' => $project['created'],
          'updated' => $project['updated'],
        ]);
        $query->execute();
        $projects[] = $project;
      }

      // Invoke freeagent_fetch_projects hook.
      $this->moduleHandler->invokeAll('freeagent_fetch_projects', [$projects]);

      // For paginated responses, grab the next page of results.
      $next_url = $this->getNextPageLink($response['headers']);
      if ($next_url) {
        $success = $this->fetchProjects($last_update, $num_requests, $next_url);
      }
    }
    else {
      $success = FALSE;
      $code = empty($response['code']) ? 'no-code' : $response['code'];
      $result = empty($response['result']) ? '' : $response['result'];
      $this->logger->error('Error fetching projects %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]);
      $this->messenger->addError(t('Error fetching projects: %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]));
    }

    return $success;
  }

  /**
   * Grab and update all tasks.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   * @param $num_requests
   *   Tracks the number of requests performed.
   * @param $url
   *   URL to fetch tasks from - required for recursive behaviour.
   */
  public function fetchTasks($last_update, &$num_requests = 0, $url = NULL) {
    /*
    $success = TRUE;
    $query = $this->connection->select('freeagent_projects', 'p');
    $query->fields('p', array('id'));
    foreach ($query->execute()->fetchCol() as $project_id) {
      $success = $this->fetchProjectTasks($last_update, $num_requests, $project_id);
    }
    */
    $success = $this->fetchProjectTasks($last_update, $num_requests);
    return $success;
  }


  /**
   * Grab and update all tasks for a particular project.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   * @param $num_requests
   *   Tracks the number of requests performed.
   * @param $project_id
   *   Project to fetch tasks for.
   * @param $url
   *   URL to fetch tasks from - required for recursive behaviour.
   */
  public function fetchProjectTasks($last_update, &$num_requests = 0, $project_id = 0, $url = NULL) {
    $success = TRUE;
    $updated_since = date('c', $last_update);
    if ($project_id) {
      $project = $this->endpoint . '/v2/projects/' . $project_id;
      $url = empty($url) ? $this->endpoint . "/v2/tasks?per_page=100&updated_since=$updated_since&project=$project" : $url;
    }
    else {
      $url = empty($url) ? $this->endpoint . "/v2/tasks?per_page=100&updated_since=$updated_since&view=all" : $url;
    }

    $num_requests++;
    $response = $this->request($url, 'GET', $num_requests);

    // Successful response.
    if (is_array($response) && $response['code'] == 200) {
      $tasks = [];
      foreach ($response['result']['tasks'] as $i => $task) {
        $task['id'] = substr(strrchr($task['url'], '/'), 1);
        $task['project_id'] = substr(strrchr($task['project'], '/'), 1);
        $task['created'] = $this->parseDate($task['created_at']);
        $task['updated'] = $this->parseDate($task['updated_at']);

        // Write the record.
        $query = $this->connection->merge('freeagent_tasks');
        $query->key('id', $task['id']);
        $query->fields([
          'name' => isset($task['name']) ? $task['name'] : '',
          'project_id' => $task['project_id'],
          'status' => isset($task['status']) ? $task['status'] : '',
          'billing_period' => isset($task['billing_period']) ? $task['billing_period'] : '',
          'is_billable' => !empty($task['is_billable']) ? $task['is_billable'] : 0,
          'billing_rate' => !empty($task['billing_rate']) ? $task['billing_rate'] : 0,
          'created' => $task['created'],
          'updated' => $task['updated'],
        ]);
        $query->execute();
        $tasks[] = $task;
      }

      // Invoke freeagent_fetch_tasks hook.
      $this->moduleHandler->invokeAll('freeagent_fetch_tasks', [$tasks]);

      // For paginated responses, grab the next page of results.
      $next_url = $this->getNextPageLink($response['headers']);
      if ($next_url) {
        $success = $this->fetchProjectTasks($last_update, $num_requests, $project_id, $next_url);
      }
    }
    else {
      $success = FALSE;
      $code = empty($response['code']) ? 'no-code' : $response['code'];
      $result = empty($response['result']) ? '' : $response['result'];
      $this->logger->error('Error fetching project tasks %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]);
      $this->messenger->addError(t('Error fetching project tasks: %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]));
    }

    return $success;
  }


  /**
   * Grab and update all timesheets from the last 90 days.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   * @param $num_requests
   *   Tracks the number of requests performed.
   * @param $url
   *   URL to fetch timesheets from - required for recursive behaviour.
   */
  public function fetchTimesheets($last_update, &$num_requests = 0, $url = NULL) {
    $success = TRUE;
    if ($last_update && empty($url)) {
      $updated_since = date('c', $last_update);
      $url = $this->endpoint . "/v2/timeslips?updated_since=$updated_since&per_page=100";
    }
    else {
      $url = empty($url) ? $this->endpoint . "/v2/timeslips?per_page=100" : $url;
    }

    $num_requests++;
    $response = $this->request($url, 'GET', $num_requests);

    // Successful response.
    if (is_array($response) && $response['code'] == 200) {
      $timeslips = [];
      foreach ($response['result']['timeslips'] as $i => $timeslip) {
        $timeslip['id'] = substr(strrchr($timeslip['url'], '/'), 1);
        $timeslip['user_id'] = substr(strrchr($timeslip['user'], '/'), 1);
        $timeslip['project_id'] = substr(strrchr($timeslip['project'], '/'), 1);
        $timeslip['task_id'] = substr(strrchr($timeslip['task'], '/'), 1);
        $timeslip['created'] = $this->parseDate($timeslip['created_at']);
        $timeslip['updated'] = $this->parseDate($timeslip['updated_at']);

        $dated_on_parts = explode('-', $timeslip['dated_on']); // 2011-08-15
        $dated_on = mktime(0, 0, 0, $dated_on_parts[1], $dated_on_parts[2], $dated_on_parts[0]);

        // Write the record.
        $query = $this->connection->merge('freeagent_timeslips');
        $query->key('id', $timeslip['id']);
        $query->fields([
          'user_id' => $timeslip['user_id'],
          'project_id' => $timeslip['project_id'],
          'task_id' => $timeslip['task_id'],
          'hours' => !empty($timeslip['hours']) ? $timeslip['hours'] : 0,
          'comment' => isset($timeslip['comment']) ? substr($timeslip['comment'], 0, 512) : '',
          'dated_on' => $dated_on,
          'day' => $dated_on_parts[2],
          'month' => $dated_on_parts[1],
          'year' => $dated_on_parts[0],
          'created' => $timeslip['created'],
          'updated' => $timeslip['updated'],
        ]);
        $query->execute();
        $timeslips[] = $timeslip;
      }

      // Invoke freeagent_fetch_timeslips hook.
      $this->moduleHandler->invokeAll('freeagent_fetch_timeslips', [$timeslips]);

      // For paginated responses, grab the next page of results.
      $next_url = $this->getNextPageLink($response['headers']);
      if ($next_url) {
        $success = $this->fetchTimesheets($last_update, $num_requests, $next_url);
      }
    }
    else {
      $success = FALSE;
      $code = empty($response['code']) ? 'no-code' : $response['code'];
      $result = empty($response['result']) ? '' : $response['result'];
      $this->logger->error('Error fetching timesheets %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]);
      $this->messenger->addError(t('Error fetching timesheets: %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]));
    }

    return $success;
  }


  /**
   * Grab and update all invoices.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   * @param $num_requests
   *   Tracks the number of requests performed.
   * @param $url
   *   URL to fetch invoices from - required for recursive behaviour.
   */
  public function fetchInvoices($last_update, &$num_requests = 0, $url = NULL) {
    $success = TRUE;
    if ($last_update && empty($url)) {
      $updated_since = date('c', $last_update);
      $url = $this->endpoint . "/v2/invoices?sort=-updated_at&updated_since=$updated_since&per_page=100";
    }
    else {
      $url = empty($url) ? $this->endpoint . "/v2/invoices?sort=-updated_at&per_page=100" : $url;
    }

    $num_requests++;
    $response = $this->request($url, 'GET', $num_requests);

    // Successful response.
    if (is_array($response) && $response['code'] == 200) {
      $invoices = [];
      foreach ($response['result']['invoices'] as $i => $invoice) {
        $invoice['id'] = substr(strrchr($invoice['url'], '/'), 1);
        $invoice['contact_id'] = substr(strrchr($invoice['contact'], '/'), 1);
        $invoice['created'] = $this->parseDate($invoice['created_at']);
        $invoice['updated'] = $this->parseDate($invoice['updated_at']);

        $dated_on_parts = explode('-', $invoice['dated_on']); // 2011-08-15
        $dated_on = mktime(0, 0, 0, $dated_on_parts[1], $dated_on_parts[2], $dated_on_parts[0]);
        $due_on_parts = explode('-', $invoice['due_on']); // 2011-08-15
        $due_on = mktime(0, 0, 0, $due_on_parts[1], $due_on_parts[2], $due_on_parts[0]);
        if (!empty($invoice['paid_on'])) {
          $paid_on_parts = explode('-', $invoice['paid_on']); // 2011-08-15
          $paid_on = mktime(0, 0, 0, $paid_on_parts[1], $paid_on_parts[2], $paid_on_parts[0]);
        }
        else {
          $paid_on = 0;
          $paid_on_parts = [0, 0, 0];
        }

        // Write the record.
        $query = $this->connection->merge('freeagent_invoices');
        $query->key('id', $invoice['id']);
        $query->fields([
          'reference' => isset($invoice['reference']) ? $invoice['reference'] : '',
          'contact_id' => $invoice['contact_id'],
          'status' => isset($invoice['status']) ? $invoice['status'] : '',
          'currency' => isset($invoice['currency']) ? $invoice['currency'] : '',
          'exchange_rate' => !empty($invoice['exchange_rate']) ? $invoice['exchange_rate'] : 0,
          'net_value' => !empty($invoice['net_value']) ? $invoice['net_value'] : 0,
          'sales_tax_value' => !empty($invoice['sales_tax_value']) ? $invoice['sales_tax_value'] : 0,
          'total_value' => !empty($invoice['total_value']) ? $invoice['total_value'] : 0,
          'paid_value' => !empty($invoice['paid_value']) ? $invoice['paid_value'] : 0,
          'due_value' => !empty($invoice['due_value']) ? $invoice['due_value'] : 0,
          'comments' => isset($invoice['comments']) ? $invoice['comments'] : '',
          'payment_terms_in_days' => !empty($invoice['payment_terms_in_days']) ? $invoice['payment_terms_in_days'] : 0,
          'ec_status' => isset($invoice['ec_status']) ? $invoice['ec_status'] : '',
          'omit_header' => !empty($invoice['omit_header']) ? $invoice['omit_header'] : 0,
          'always_show_bic_and_iban' => !empty($invoice['always_show_bic_and_iban']) ? $invoice['always_show_bic_and_iban'] : 0,
          'show_project_name' => !empty($invoice['show_project_name']) ? $invoice['show_project_name'] : 0,
          'dated_on' => $dated_on,
          'day' => $dated_on_parts[2],
          'month' => $dated_on_parts[1],
          'year' => $dated_on_parts[0],
          'due_on' => $due_on,
          'due_day' => $due_on_parts[2],
          'due_month' => $due_on_parts[1],
          'due_year' => $due_on_parts[0],
          'paid_on' => $paid_on,
          'paid_day' => $paid_on_parts[2],
          'paid_month' => $paid_on_parts[1],
          'paid_year' => $paid_on_parts[0],
          'created' => $invoice['created'],
          'updated' => $invoice['updated'],
        ]);
        $query->execute();
        $invoices[] = $invoice;
      }

      // Invoke freeagent_fetch_invoices hook.
      $this->moduleHandler->invokeAll('freeagent_fetch_invoices', [$invoices]);

      // For paginated responses, grab the next page of results.
      $next_url = $this->getNextPageLink($response['headers']);
      if ($next_url) {
        $success = $this->fetchInvoices($last_update, $num_requests, $next_url);
      }
    }
    else {
      $success = FALSE;
      $code = empty($response['code']) ? 'no-code' : $response['code'];
      $result = empty($response['result']) ? '' : $response['result'];
      $this->logger->error('Error fetching invoices %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]);
      $this->messenger->addError(t('Error fetching invoices: %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]));
    }

    return $success;
  }

  /**
   * Grab and update all categories.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   * @param $num_requests
   *   Tracks the number of requests performed.
   * @param $url
   *   URL to fetch expense categories from - required for recursive behaviour.
   */
  public function fetchCategories($last_update, &$num_requests = 0, $url = NULL) {
    $success = TRUE;
    $url = empty($url) ? $this->endpoint . "/v2/categories?per_page=100" : $url;

    $num_requests++;
    $response = $this->request($url, 'GET', $num_requests);

    // Successful response.
    if (is_array($response) && $response['code'] == 200) {
      $categories = [];
      foreach ($response['result']['admin_expenses_categories'] as $i => $category) {
        $category['id'] = substr(strrchr($category['url'], '/'), 1);

        // Write the record.
        $query = $this->connection->merge('freeagent_categories');
        $query->key('id', $category['id']);
        $query->fields([
          'description' => isset($category['description']) ? $category['description'] : '',
          'tax_reporting_name' => isset($category['tax_reporting_name']) ? $category['tax_reporting_name'] : '',
          'nominal_code' => isset($category['nominal_code']) ? $category['nominal_code'] : '',
          'allowable_for_tax' => !empty($category['allowable_for_tax']) ? $category['allowable_for_tax'] : 0,
          'auto_sales_tax_rate' => isset($category['auto_sales_tax_rate']) ? $category['auto_sales_tax_rate'] : '',
          'category_type' => 'admin_expenses_categories',
        ]);
        $query->execute();
        $categories[] = $category;
      }
      foreach ($response['result']['cost_of_sales_categories'] as $i => $category) {
        $category['id'] = substr(strrchr($category['url'], '/'), 1);

        // Write the record.
        $query = $this->connection->merge('freeagent_categories');
        $query->key('id', $category['id']);
        $query->fields([
          'description' => isset($category['description']) ? $category['description'] : '',
          'tax_reporting_name' => isset($category['tax_reporting_name']) ? $category['tax_reporting_name'] : '',
          'nominal_code' => isset($category['nominal_code']) ? $category['nominal_code'] : '',
          'allowable_for_tax' => !empty($category['allowable_for_tax']) ? $category['allowable_for_tax'] : 0,
          'auto_sales_tax_rate' => isset($category['auto_sales_tax_rate']) ? $category['auto_sales_tax_rate'] : '',
          'category_type' => 'cost_of_sales_categories',
        ]);
        $query->execute();
        $categories[] = $category;
      }
      foreach ($response['result']['income_categories'] as $i => $category) {
        $category['id'] = substr(strrchr($category['url'], '/'), 1);

        // Write the record.
        $query = $this->connection->merge('freeagent_categories');
        $query->key('id', $category['id']);
        $query->fields([
          'description' => isset($category['description']) ? $category['description'] : '',
          'tax_reporting_name' => isset($category['tax_reporting_name']) ? $category['tax_reporting_name'] : '',
          'nominal_code' => isset($category['nominal_code']) ? $category['nominal_code'] : '',
          'allowable_for_tax' => !empty($category['allowable_for_tax']) ? $category['allowable_for_tax'] : 0,
          'auto_sales_tax_rate' => isset($category['auto_sales_tax_rate']) ? $category['auto_sales_tax_rate'] : '',
          'category_type' => 'income_categories',
        ]);
        $query->execute();
        $categories[] = $category;
      }
      foreach ($response['result']['general_categories'] as $i => $category) {
        $category['id'] = substr(strrchr($category['url'], '/'), 1);

        // Write the record.
        $query = $this->connection->merge('freeagent_categories');
        $query->key('id', $category['id']);
        $query->fields([
          'description' => isset($category['description']) ? $category['description'] : '',
          'tax_reporting_name' => isset($category['tax_reporting_name']) ? $category['tax_reporting_name'] : '',
          'nominal_code' => isset($category['nominal_code']) ? $category['nominal_code'] : '',
          'allowable_for_tax' => !empty($category['allowable_for_tax']) ? $category['allowable_for_tax'] : 0,
          'auto_sales_tax_rate' => isset($category['auto_sales_tax_rate']) ? $category['auto_sales_tax_rate'] : '',
          'category_type' => 'general_categories',
        ]);
        $query->execute();
        $categories[] = $category;
      }

      // Invoke freeagent_fetch_categories hook.
      $this->moduleHandler->invokeAll('freeagent_fetch_categories', [$categories]);

      // For paginated responses, grab the next page of results.
      $next_url = $this->getNextPageLink($response['headers']);
      if ($next_url) {
        $success = $this->fetchCategories($last_update, $num_requests, $next_url);
      }
    }
    else {
      $success = FALSE;
      $code = empty($response['code']) ? 'no-code' : $response['code'];
      $result = empty($response['result']) ? '' : $response['result'];
      $this->logger->error('Error fetching categories %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]);
      $this->messenger->addError(t('Error fetching categories: %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]));
    }

    return $success;
  }


  /**
   * Grab and update all expenses for a given period.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   * @param $num_requests
   *   Tracks the number of requests performed.
   * @param $url
   *   URL to fetch expenses from - required for recursive behaviour.
   */
  public function fetchExpenses($last_update, &$num_requests = 0, $url = NULL) {
    $success = TRUE;
    if ($last_update && empty($url)) {
      $updated_since = date('c', $last_update);
      $url = $this->endpoint . "/v2/expenses?updated_since=$updated_since&per_page=100";
    }
    else {
      $url = empty($url) ? $this->endpoint . "/v2/expenses?per_page=100" : $url;
    }

    $num_requests++;
    $response = $this->request($url, 'GET', $num_requests);

    // Successful response.
    if (is_array($response) && $response['code'] == 200) {
      $expenses = [];
      foreach ($response['result']['expenses'] as $i => $expense) {
        $expense['id'] = substr(strrchr($expense['url'], '/'), 1);
        $expense['user_id'] = substr(strrchr($expense['user'], '/'), 1);
        $expense['category_id'] = substr(strrchr($expense['category'], '/'), 1);
        $expense['created'] = $this->parseDate($expense['created_at']);
        $expense['updated'] = $this->parseDate($expense['updated_at']);

        $dated_on_parts = explode('-', $expense['dated_on']); // 2011-08-15
        $dated_on = mktime(0, 0, 0, $dated_on_parts[1], $dated_on_parts[2], $dated_on_parts[0]);

        // Write the record.
        $query = $this->connection->merge('freeagent_expenses');
        $query->key('id', $expense['id']);
        $query->fields([
          'user_id' => $expense['user_id'],
          'category_id' => $expense['category_id'],
          'currency' => isset($expense['currency']) ? $expense['currency'] : '',
          'gross_value' => !empty($expense['gross_value']) ? $expense['gross_value'] : 0,
          'native_gross_value' => !empty($expense['native_gross_value']) ? $expense['native_gross_value'] : 0,
          'sales_tax_rate' => !empty($expense['sales_tax_rate']) ? $expense['sales_tax_rate'] : 0,
          'sales_tax_value' => !empty($expense['sales_tax_value']) ? $expense['sales_tax_value'] : 0,
          'native_sales_tax_value' => !empty($expense['native_sales_tax_value']) ? $expense['native_sales_tax_value'] : 0,
          'manual_sales_tax_amount' => !empty($expense['manual_sales_tax_amount']) ? $expense['manual_sales_tax_amount'] : 0,
          'mileage' => !empty($expense['mileage']) ? $expense['mileage'] : 0,
          'engine_size_index' => !empty($expense['engine_size_index']) ? $expense['engine_size_index'] : 0,
          'engine_type_index' => !empty($expense['engine_type_index']) ? $expense['engine_type_index'] : 0,
          'initial_rate_mileage' => !empty($expense['initial_rate_mileage']) ? $expense['initial_rate_mileage'] : 0,
          'have_vat_receipt' => !empty($expense['have_vat_receipt']) ? $expense['have_vat_receipt'] : 0,
          'description' => isset($expense['description']) ? $expense['description'] : '',
          'dated_on' => $dated_on,
          'day' => $dated_on_parts[2],
          'month' => $dated_on_parts[1],
          'year' => $dated_on_parts[0],
          'created' => $expense['created'],
          'updated' => $expense['updated'],
        ]);
        $query->execute();
        $expenses[] = $expense;
      }

      // Invoke freeagent_fetch_expenses hook.
      $this->moduleHandler->invokeAll('freeagent_fetch_expenses', [$expenses]);

      // For paginated responses, grab the next page of results.
      $next_url = $this->getNextPageLink($response['headers']);
      if ($next_url) {
        $success = $this->fetchExpenses($last_update, $num_requests, $next_url);
      }
    }
    else {
      $success = FALSE;
      $code = empty($response['code']) ? 'no-code' : $response['code'];
      $result = empty($response['result']) ? '' : $response['result'];
      $this->logger->error('Error fetching expenses %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]);
      $this->messenger->addError(t('Error fetching expenses: %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]));
    }

    return $success;
  }

  /**
   * Grab and update all bills.
   *
   * @param $last_update
   *   Timestamp of when data was refreshed last.
   * @param $num_requests
   *   Tracks the number of requests performed.
   * @param $url
   *   URL to fetch bills from - required for recursive behaviour.
   */
  public function fetchBills($last_update, &$num_requests = 0, $url = NULL) {
    $success = TRUE;
    if ($last_update && empty($url)) {
      $updated_since = date('c', $last_update);
      $url = $this->endpoint . "/v2/bills?updated_since=$updated_since&per_page=100";
    }
    else {
      $url = empty($url) ? $this->endpoint . "/v2/bills?per_page=100" : $url;
    }

    $num_requests++;
    $response = $this->request($url, 'GET', $num_requests);

    // Successful response.
    if (is_array($response) && $response['code'] == 200) {
      $bills = [];
      foreach ($response['result']['bills'] as $i => $bill) {
        $bill['id'] = substr(strrchr($bill['url'], '/'), 1);
        $bill['contact_id'] = substr(strrchr($bill['contact'], '/'), 1);
        $bill['project_id'] = isset($bill['project']) ? substr(strrchr($bill['project'], '/'), 1) : NULL;
        $bill['category_id'] = substr(strrchr($bill['category'], '/'), 1);
        $bill['created'] = $this->parseDate($bill['created_at']);
        $bill['updated'] = $this->parseDate($bill['updated_at']);

        $dated_on_parts = explode('-', $bill['dated_on']); // 2011-08-15
        $dated_on = mktime(0, 0, 0, $dated_on_parts[1], $dated_on_parts[2], $dated_on_parts[0]);
        $due_on_parts = explode('-', $bill['due_on']); // 2011-08-15
        $due_on = mktime(0, 0, 0, $due_on_parts[1], $due_on_parts[2], $due_on_parts[0]);

        // Write the record.
        $query = $this->connection->merge('freeagent_bills');
        $query->key('id', $bill['id']);
        $query->fields([
          'contact_id' => $bill['contact_id'],
          'project_id' => $bill['project_id'],
          'category_id' => $bill['category_id'],
          'reference' => isset($bill['reference']) ? $bill['reference'] : '',
          'status' => isset($bill['status']) ? $bill['status'] : '',
          'total_value' => !empty($bill['total_value']) ? $bill['total_value'] : 0,
          'paid_value' => !empty($bill['paid_value']) ? $bill['paid_value'] : 0,
          'due_value' => !empty($bill['due_value']) ? $bill['due_value'] : 0,
          'sales_tax_value' => !empty($bill['sales_tax_value']) ? $bill['sales_tax_value'] : 0,
          'sales_tax_rate' => !empty($bill['sales_tax_rate']) ? $bill['sales_tax_rate'] : 0,
          'dated_on' => $dated_on,
          'day' => $dated_on_parts[2],
          'month' => $dated_on_parts[1],
          'year' => $dated_on_parts[0],
          'due_on' => $due_on,
          'due_day' => $due_on_parts[2],
          'due_month' => $due_on_parts[1],
          'due_year' => $due_on_parts[0],
          'created' => $bill['created'],
          'updated' => $bill['updated'],
        ]);
        $query->execute();
        $bills[] = $bill;
      }

      // Invoke freeagent_fetch_bills hook.
      $this->moduleHandler->invokeAll('freeagent_fetch_bills', [$bills]);

      // For paginated responses, grab the next page of results.
      $next_url = $this->getNextPageLink($response['headers']);
      if ($next_url) {
        $success = $this->fetchBills($last_update, $num_requests, $next_url);
      }
    }
    else {
      $success = FALSE;
      $code = empty($response['code']) ? 'no-code' : $response['code'];
      $result = empty($response['result']) ? '' : $response['result'];
      $this->logger->error('Error fetching bills %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]);
      $this->messenger->addError(t('Error fetching bills: %code: %message', [
        '%code' => $code,
        '%message' => $result,
      ]));
    }

    return $success;
  }

  /**
   * Retrieve a list of FreeAgent users from the database.
   */
  public function getUsersInfo($rebuild = FALSE) {
    $cid = 'freeagent_users';

    if (!$rebuild) {
      $_freeagent_users = &drupal_static(__FUNCTION__);
      if (isset($_freeagent_users)) {
        return $_freeagent_users;
      }
      if ($cache = $this->cache->get($cid)) {
        $_freeagent_users = $cache->data;
        return $_freeagent_users;
      }
    }

    $_freeagent_users = [];

    $query = $this->connection->select('freeagent_users', 'f');
    $query->fields('f');
    $query->orderBy('f.id', 'ASC');
    foreach ($query->execute() as $user_object) {
      $_freeagent_users[$user_object->id] = $user_object;
    }

    $this->cache->set($cid, $_freeagent_users);

    return $_freeagent_users;
  }

  /**
   * Retrieve a list of FreeAgent contacts from the database.
   */
  public function getContactsInfo($rebuild = FALSE) {
    $cid = 'freeagent_contacts';

    if (!$rebuild) {
      $_freeagent_contacts = &drupal_static(__FUNCTION__);
      if (isset($_freeagent_contacts)) {
        return $_freeagent_contacts;
      }
      if ($cache = $this->cache->get($cid)) {
        $_freeagent_contacts = $cache->data;
        return $_freeagent_contacts;
      }
    }

    $_freeagent_contacts = [];

    $query = $this->connection->select('freeagent_contacts', 'f');
    $query->fields('f');
    $query->orderBy('f.id', 'ASC');
    foreach ($query->execute() as $contact) {
      $contact->contact_name = !empty($contact->organisation_name) ? $contact->organisation_name : $contact->first_name . ' ' . $contact->last_name;
      $_freeagent_contacts[$contact->id] = $contact;
    }

    $this->cache->set($cid, $_freeagent_contacts);

    return $_freeagent_contacts;
  }


  /**
   * Retrieve a list of FreeAgent projects from the database.
   */
  public function getProjectsInfo($rebuild = FALSE, $contact_id = NULL) {
    $cid = 'freeagent_projects';

    if (!$rebuild) {
      $_freeagent_projects = &drupal_static(__FUNCTION__);
      if (isset($_freeagent_projects)) {
        return empty($contact_id) ? $_freeagent_projects->projects : (empty($_freeagent_projects->contacts[$contact_id]) ? [] : $_freeagent_projects->contacts[$contact_id]);
      }
      if ($cache = $this->cache->get($cid)) {
        $_freeagent_projects = $cache->data;
        return empty($contact_id) ? $_freeagent_projects->projects : (empty($_freeagent_projects->contacts[$contact_id]) ? [] : $_freeagent_projects->contacts[$contact_id]);
      }
    }

    $_freeagent_projects = new \stdClass();
    $_freeagent_projects->contacts = [];
    $_freeagent_projects->projects = [];

    $query = $this->connection->select('freeagent_projects', 'fp');
    $query->fields('fp');
    $query->orderBy('fp.id', 'ASC');
    $query->join('freeagent_contacts', 'fc', 'fp.contact_id = fc.id');
    $query->addField('fc', 'organisation_name', 'organisation_name');
    $query->addField('fc', 'first_name', 'first_name');
    $query->addField('fc', 'last_name', 'last_name');
    foreach ($query->execute() as $project) {
      $project->contact_name = !empty($project->organisation_name) ? $project->organisation_name : $project->first_name . ' ' . $project->last_name;
      $_freeagent_projects->contacts[$project->contact_id][$project->id] = $project;
      $_freeagent_projects->projects[$project->id] = $project;
    }

    $this->cache->set($cid, $_freeagent_projects);

    return empty($contact_id) ? $_freeagent_projects->projects : (empty($_freeagent_projects->contacts[$contact_id]) ? [] : $_freeagent_projects->contacts[$contact_id]);
  }

  /**
   * Retrieve a list of FreeAgent tasks from the database.
   */
  public function getTasksInfo($rebuild = FALSE, $project_id = NULL) {
    $cid = 'freeagent_tasks';

    if (!$rebuild) {
      $_freeagent_tasks = &drupal_static(__FUNCTION__);
      if (isset($_freeagent_tasks)) {
        return empty($project_id) ? $_freeagent_tasks->tasks : (empty($_freeagent_tasks->projects[$project_id]) ? [] : $_freeagent_tasks->projects[$project_id]);
      }
      if ($cache = $this->cache->get($cid)) {
        $_freeagent_tasks = $cache->data;
        return empty($project_id) ? $_freeagent_tasks->tasks : (empty($_freeagent_tasks->projects[$project_id]) ? [] : $_freeagent_tasks->projects[$project_id]);
      }
    }

    $_freeagent_tasks = new \stdClass();
    $_freeagent_tasks->projects = [];
    $_freeagent_tasks->tasks = [];

    $query = $this->connection->select('freeagent_tasks', 'ft');
    $query->fields('ft');
    $query->orderBy('ft.id', 'ASC');
    $query->join('freeagent_projects', 'fp', 'ft.project_id = fp.id');
    $query->addField('fp', 'name', 'project_name');
    $query->join('freeagent_contacts', 'fc', 'fp.contact_id = fc.id');
    $query->addField('fc', 'organisation_name', 'organisation_name');
    $query->addField('fc', 'first_name', 'first_name');
    $query->addField('fc', 'last_name', 'last_name');
    foreach ($query->execute() as $task) {
      $task->contact_name = !empty($task->organisation_name) ? $task->organisation_name : $task->first_name . ' ' . $task->last_name;
      $_freeagent_tasks->projects[$task->project_id][$task->id] = $task;
      $_freeagent_tasks->tasks[$task->id] = $task;
    }

    $this->cache->set($cid, $_freeagent_tasks);

    return empty($project_id) ? $_freeagent_tasks->tasks : (empty($_freeagent_tasks->projects[$project_id]) ? [] : $_freeagent_tasks->projects[$project_id]);
  }

  /**
   * Retrieve a list of FreeAgent categories from the database.
   */
  public function getCategoriesInfo($rebuild = FALSE) {
    $cid = 'freeagent_categories';

    if (!$rebuild) {
      $_freeagent_categories = &drupal_static(__FUNCTION__);
      if (isset($_freeagent_categories)) {
        return $_freeagent_categories;
      }
      if ($cache = $this->cache->get($cid)) {
        $_freeagent_categories = $cache->data;
        return $_freeagent_categories;
      }
    }

    $_freeagent_categories = [];

    $query = $this->connection->select('freeagent_categories', 'f');
    $query->fields('f');
    $query->orderBy('f.id', 'ASC');
    foreach ($query->execute() as $category) {
      $_freeagent_categories[$category->id] = $category;
    }

    $this->cache->set($cid, $_freeagent_categories);

    return $_freeagent_categories;
  }

  /**
   * Create a new user in FreeAgent.
   */
  public function createUser($email, $first_name, $last_name, $role, $permission_level, $notify = FALSE) {
    $success = TRUE;
    $num_requests = 0;
    $debug = $this->state->get('freeagent.debug', FALSE);

    // Ensure we have no other user with the same name already.
    $match = $this->connection->select('freeagent_users', 'u')
      ->fields('u', ['id'])
      ->condition('u.email', $email, '=')
      ->execute()
      ->fetchField();
    if (!empty($match)) {
      $success = FALSE;
      $this->logger->error('Error adding user %email: existing user with the same email, id: %id', array('%email' => $email, '%id' => $match));
      $this->messenger->addError(t('Error adding user %email: existing user with the same email, id: %id', array('%email' => $email, '%id' => $match)));
      return $success;
    }

    $allowed_roles = ['Owner', 'Director', 'Partner', 'Company Secretary', 'Employee', 'Shareholder', 'Accountant'];
    if (!in_array($role, $allowed_roles)) {
      $role = 'Employee';
    }

    $params = [
      'user' => [
        'email' => $email,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'last_name' => $first_name . ' ' . $last_name,
        'role' => $role,
        'opening_mileage' => 0,
        'permission_level' => $permission_level,
        'send_invitation' => $notify,
      ],
    ];

    $url = $this->endpoint . '/v2/users';
    if ($debug) $this->logger->debug('url: @url', array('@url' => $url));
    if ($debug) $this->logger->debug('params: @params', array('@params' => $params));

    $num_requests = 0;
    $response = $this->request($url, 'POST', $num_requests, $params);

    // Successful response.
    if (empty($response['code']) || $response['code'] != 201) {
      $success = FALSE;
      $code = empty($response['code']) ? 'no-code' : $response['code'];
      $result = empty($response['result']) ? '' : $response['result'];
      $result = is_array($result['errors']) ? $result['errors']['error']['message'] : $result;

      $this->logger->error('Error adding user %email: %code: %message', array('%email' => $email, '%code' => $code, '%message' => $result));
      $this->messenger->addError(t('Error adding user %email: %code: %message', array('%email' => $email, '%code' => $code, '%message' => $result)));
      return $success;
    }

    // Retrieve the user id.
    $user_url = $response['result']['user']['url'];
    $user_id = preg_replace('/https:\/\/api.(sandbox.)*freeagent.com\/v2\/users\//', '', $user_url);

    // Add it to the freeagent_users table.
    $now = time();
    $this->connection->insert('freeagent_users')
      ->fields([
        'id' => $user_id,
        'email' => $email,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'full_name' => $first_name . ' ' . $last_name,
        'role' => $role,
        'permission_level' => $permission_level,
        'opening_mileage' => 0,
        'created' => $now,
        'updated' => $now,
      ])
      ->execute();

    return $user_id;
  }
}

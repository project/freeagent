<?php

namespace Drupal\freeagent\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\freeagent\Services\FreeAgent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides configuration form for FreeAgent API module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The FreeAgent service.
   *
   * @var \Drupal\freeagent\Services\FreeAgent
   */
  protected $freeAgent;

  /**
   * Constructs a new SettingsForm instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store.
   * @param \Drupal\freeagent\Services\FreeAgent $freeagent
   *   The FreeAgent service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state, FreeAgent $freeagent) {
    parent::__construct($config_factory);

    $this->state = $state;
    $this->freeAgent = $freeagent;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('freeagent')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'freeagent_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'freeagent.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('freeagent.settings');

    $form['use_sandbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use sandbox'),
      '#default_value' => $config->get('use_sandbox'),
      '#description' => $this->t('If enabled, it will authenticate against the sandbox endpoint, otherwise the production endpoint will be used.'),
    ];
    // Configure account credentials.
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your FreeAgent app client id'),
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];
    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your FreeAgent app client secret'),
      '#default_value' => $config->get('client_secret'),
      '#required' => TRUE,
    ];

    // Display message about authorisation.
    $access_token = $this->state->get('freeagent.access_token', '');
    if (!empty($access_token)) {
      $message = $this->t('Your FreeAgent access token is: @token', ['@token' => $access_token]);
    }
    else {
      $message = $this->t('Not successfully authorised yet.');
    }
    $form['verify'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $message . '</p>',
    ];

    $form['fetch_data_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Additional data to fetch'),
      '#description' => $this->t('By default, the integration will fetch contacts, projects and tasks. Here you can choose additional data to retrieve from FreeAgent.'),
      '#options' => [
        'timeslips' => $this->t('Timeslips'),
        'expenses' => $this->t('Expenses'),
        'invoices' => $this->t('Invoices'),
        'bills' => $this->t('Bills'),
      ],
      '#default_value' => $config->get('fetch_data_types'),
    ];

    $form['purge_all_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Purge all data'),
    ];
    $form['refresh_all_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Refresh all data'),
      '#description' => $this->t('All data is refreshed, including old data, and not just new records retrieved. This may take some time.'),
    ];

    $form['actions']['authorise'] = [
      '#type' => 'submit',
      '#value' => $this->t('Authorise'),
      '#submit' => ['::authorise'],
    ];
    $form['actions']['refresh'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh data'),
      '#submit' => ['::submitForm', '::refresh'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('freeagent.settings');
    $config->set('use_sandbox', $form_state->getValue('use_sandbox'));
    $config->set('client_id', trim($form_state->getValue('client_id')));
    $config->set('client_secret', trim($form_state->getValue('client_secret')));
    $config->set('fetch_data_types', array_filter($form_state->getValue('fetch_data_types')));
    $config->save();

    if (!empty($form_state->getValue('purge_all_data'))) {
      $this->freeAgent->purgeData();
      $this->messenger->addStatus($this->t('All data has been deleted.'));
    }
    else {
      $purge_types = [];
      foreach ($form_state->getValue('fetch_data_types') as $type => $keep) {
        if (empty($keep)) {
          $purge_types[] = $type;
        }
      }
      if (!empty($purge_types)) {
        $this->freeAgent->purgeData($purge_types);
      }
    }
    if (!empty($form_state->getValue('refresh_all_data'))) {
      $this->freeAgent->refreshData(86400);
    }
  }

  /**
   * Submit handler to authorise the app.
   */
  public function authorise(array &$form, FormStateInterface $form_state) {
    $response = $this->freeAgent->authorise();
    if (!empty($response)) {
      $form_state->setResponse($response);
    }
  }

  /**
   * Submit handler to refresh data.
   */
  public function refresh(array &$form, FormStateInterface $form_state) {
    // Refreshes ALL data, including older data.
    if (!empty($form_state->getValue('refresh_all_data'))) {
      $this->freeAgent->refreshData(86400);
    }
    // Only pull down the latest data since the last refresh.
    else {
      $this->freeAgent->refreshData(0);
    }
  }

}

# FreeAgent API

## INTRODUCTION

This module integrates Drupal with the online accounting software, FreeAgent.

## REQUIREMENTS

At time of writing, the patch from
[#2647292](https://www.drupal.org/project/drupal/issues/2647292) in order for
date filtering to work correctly on the Views provided by freeagent\_reports
module.

## INSTALLATION

1. The module can be installed via the
   [standard Drupal installation process](http://drupal.org/node/1897420).
2. Merge the following with your project's composer.json file.
   ```
   "repositories": [
     {
       "type": "package",
       "package": {
         "name": "snpower/php-oauth2",
         "version": "master",
         "type": "library",
         "dist": {
           "type": "zip",
           "url": "https://github.com/snpower/PHP-OAuth2/archive/master.zip"
         },
         "require": {
           "composer/installers": "~1.0"
         }
       }
     }
   ],
   "autoload": {
     "psr-4": {
       "OAuth2\\": "vendor/snpower/php-oauth2/"
     }
   }
   ```
3. Run following command to download required PHP library.
   ```
   composer require snpower/php-oauth2:master
   ```

If you want more than just the downloading of FreeAgent data and want to run
reports on timeslips entered, then enable the FreeAgent reports sub-module. This
has dependencies on Views, Views Data Export (for csv exports) and Charts (for
bar charts, etc) modules.

It also has a dependency on this patch to the Views module:
https://drupal.org/node/1182256

## CONFIGURATION

1. Enable API access in your FreeAgent account at
   https://yourcompany.freeagent.com/settings/api
2. Create a developer account at https://dev.freeagent.com/apps
3. Create a new app and set the 'Url' to be your website's url. You can leave
   the 'Oauth redirect url' blank.
4. Go to admin/config/services/freeagent and enter in the client id
   and secret that were generated when you registered your app and click the
   'Authorise' button.

## MAINTAINERS

This module is maintained by developers at Annertech. For more information on
the company and our offerings, see [annertech.com](http://annertech.com).

/**
 * @file
 * Reduce the values of project and task drop-down filters when clients and/or
 * projects are selected.
 */

(function ($, Drupal) {

  Drupal.behaviors.freeagentReportProjectTaskFilter = {
    attach: function (context) {

      var client_select = $('#edit-id-1');
      var project_select = $('#edit-id-2');

      var client_selected = client_select.find('option:selected').val();
      var project_selected = project_select.find('option:selected').val();

      // Change projects listed when the page is loaded.
      if (client_selected != 'All') {
        updateProjects(client_select);
      }

      // Change tasks listed when the page is loaded.
      if (project_selected != 'All') {
        updateTasks(project_select);
      }

      // Change projects listed when a client is selected.
      client_select.change(function () {
        updateProjects($(this));
      });

      // Change tasks listed when a project is selected.
      project_select.change(function () {
        updateTasks($(this));
      });

    }
  }

  // React to the client drop-down changing by updating projects and associated
  // tasks.
  function updateProjects(this_select) {
    var projects_url = '/freeagent/projects/';
    var tasks_client_url = '/freeagent/tasks/client/';

    // Target selectbox to apply modifications on it
    var client_select = $('#edit-id-1');
    var project_select = $('#edit-id-2');
    var task_select = $('#edit-id-3');

    // Save first dummy/null option
    var client_any_option = client_select.find('option:first');
    var project_any_option = project_select.find('option:first');
    var project_selected = project_select.find('option:selected').val();
    var task_any_option = task_select.find('option:first');
    var task_selected = task_select.find('option:selected').val();

    // Update projects list.
    $.ajax({
      type: 'GET',
      url: projects_url + this_select.val(),
      dataType: 'json',
      beforeSend: function () {
        // Disable selectbox
        project_select.attr('disabled', 'disabled');
      },
      success: function (data) {
        // Insert saved first dummy/null option
        project_select.empty().append(project_any_option);
        $.each(data, function (id, name) {
          // Append html data to target dropdown element
          if (id == project_selected) {
            project_select.append('<option selected="selected" value="' + id + '">' + name + '</option>');
          }
          else {
            project_select.append('<option value="' + id + '">' + name + '</option>');
          }
        });
      },
      complete: function () {
        // Re-enable selectbox
        project_select.removeAttr('disabled');
      }
    });

    // Update tasks list.
    $.ajax({
      type: 'GET',
      url: tasks_client_url + this_select.val(),
      dataType: 'json',
      beforeSend: function () {
        // Disable selectbox
        task_select.attr('disabled', 'disabled');
      },
      success: function (data) {
        // Insert saved first dummy/null option
        task_select.empty().append(task_any_option);
        $.each(data, function (id, name) {
          // Append html data to target dropdown element
          if (id == task_selected) {
            task_select.append('<option selected="selected" value="' + id + '">' + name + '</option>');
          }
          else {
            task_select.append('<option value="' + id + '">' + name + '</option>');
          }
        });
      },
      complete: function () {
        // Re-enable selectbox
        task_select.removeAttr('disabled');
      }
    });
  }

  function updateTasks(this_select) {
    var tasks_project_url = '/freeagent/tasks/project/';

    // Target selectbox to apply modifications on it
    var project_select = $('#edit-id-2');
    var task_select = $('#edit-id-3');

    // Save first dummy/null option
    var project_any_option = project_select.find('option:first');
    var task_any_option = task_select.find('option:first');
    var task_selected = task_select.find('option:selected').val();

    $.ajax({
      type: 'GET',
      url: tasks_project_url + this_select.val(),
      dataType: 'json',
      beforeSend: function () {
        // Disable selectbox
        task_select.attr('disabled', 'disabled');
      },
      success: function (data) {
        // Insert saved first dummy/null option
        task_select.empty().append(task_any_option);
        $.each(data, function (id, name) {
          // Append html data to target dropdown element
          if (id == task_selected) {
            task_select.append('<option selected="selected" value="' + id + '">' + name + '</option>');
          }
          else {
            task_select.append('<option value="' + id + '">' + name + '</option>');
          }
        });
      },
      complete: function () {
        // Re-enable selectbox
        task_select.removeAttr('disabled');
      }
    });
  }

})(jQuery, Drupal);

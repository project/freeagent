/**
 * @file
 * Reduce the values of project and task drop-down filters when clients and/or
 * projects are selected.
 */

(function ($, Drupal) {

  Drupal.behaviors.freeagentReportDateFilter = {
    attach: function (context) {

      // Add datepicker to the custom date range filter.
      $('#edit-custom-date-max, #edit-custom-date-min, #edit-custom-date-value, #edit-dated-on-max, #edit-dated-on-min, #edit-dated-on-value, #edit-due-on-max, #edit-due-on-min, #edit-due-on-value, #edit-paid-on-max, #edit-paid-on-min, #edit-paid-on-value, #edit-created-max, #edit-created-min, #edit-created-value').datepicker({
        dateFormat: 'yy-mm-dd',
        autoSize: false
      });

      // On load, hide/show custom date range filter.
      toggleCustomDateRange();

      // On date drop-down change, hide/show custom date range filter.
      $('#edit-dated-on').change(function () {
        toggleCustomDateRange();
      });

      function toggleCustomDateRange() {
        if ($('#edit-dated-on option:selected').html().trim() === Drupal.t('Custom')) {
          $('#edit-custom-date-wrapper').show();
        }
        else {
          $('#edit-custom-date-wrapper').hide();
          $('#edit-custom-date-min').val('');
          $('#edit-custom-date-max').val('');
        }
      }

    }
  };

})(jQuery, Drupal);

<?php

namespace Drupal\freeagent_reports\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\freeagent\Services\FreeAgent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides controllers for FreeAgent Reports module.
 */
class FreeAgentReportsController extends ControllerBase {

  /**
   * The FreeAgent service.
   *
   * @var \Drupal\freeagent\Services\FreeAgent
   */
  protected $freeAgent;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new FreeAgentReportsController instance.
   *
   * @param \Drupal\freeagent\Services\FreeAgent $freeagent
   *   The FreeAgent service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(FreeAgent $freeagent, AccountInterface $current_user) {
    $this->freeAgent = $freeagent;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('freeagent'),
      $container->get('current_user')
    );
  }

  /**
   * Provides access control for FreeAgent reports menu item.
   */
  public function reportsAccess() {
    if ($this->currentUser->hasPermission('access freeagent timesheet reports') ||
      $this->currentUser->hasPermission('access freeagent invoice reports') ||
      $this->currentUser->hasPermission('access freeagent expense reports') ||
      $this->currentUser->hasPermission('access freeagent bill reports')
    ) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * Provides list of projects for given contact.
   *
   * @param int $contact_id
   *   The contact Id.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The Ajax response.
   */
  public function ajaxProjectsByContact($contact_id) {
    $output = [];
    if (is_numeric($contact_id)) {
      $projects = $this->freeAgent->getProjectsInfo(FALSE, $contact_id);
    }
    else {
      $projects = $this->freeAgent->getProjectsInfo(FALSE);
    }
    if (!empty($projects)) {
      foreach ($projects as $id => $project) {
        $output[$id] = $project->name;
      }
    }
    return new AjaxResponse($output);
  }

  /**
   * Provides list of tasks for given contact.
   *
   * @param int $contact_id
   *   The contact Id.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The Ajax response.
   */
  public function ajaxTasksByContact($contact_id) {
    $output = [];
    if (is_numeric($contact_id)) {
      $projects = $this->freeAgent->getProjectsInfo(FALSE, $contact_id);
      $tasks = [];
      if (!empty($projects)) {
        foreach ($projects as $id => $project) {
          $project_tasks = $this->freeAgent->getTasksInfo(FALSE, $id);
          if (!empty($project_tasks)) {
            $tasks += $project_tasks;
          }
        }
      }
    }
    else {
      $tasks = $this->freeAgent->getTasksInfo(FALSE);
    }
    if (!empty($tasks)) {
      foreach ($tasks as $id => $task) {
        $output[$id] = $task->project_name . ' : ' . $task->name;
      }
    }
    return new AjaxResponse($output);
  }

  /**
   * Provides list of tasks for given project.
   *
   * @param int $project_id
   *   The project Id.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The Ajax response.
   */
  public function ajaxTasksByProject($project_id) {
    $output = [];
    if (is_numeric($project_id)) {
      $tasks = $this->freeAgent->getTasksInfo(FALSE, $project_id);
    }
    else {
      $tasks = $this->freeAgent->getTasksInfo(FALSE);
    }
    if (!empty($tasks)) {
      foreach ($tasks as $id => $task) {
        $output[$id] = $task->name;
      }
    }
    return new AjaxResponse($output);
  }

}
